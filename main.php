<?php
require_once('csvreader.php');
 
function collectParents($data, &$parentProducts) {
    $key = preg_match('/^\d\d\d\/\d\d\d/', $data[0], $matches);
    $key = $matches[0];
    if ($key) {
        $parentProducts[$key]['childrens'][] = $data[0];
        $parentProducts[$key]['name'] = $data[2];
        if ($data[4] != 0 && floatval($data[4]) < floatval($parentProducts[$key]['price'])) {
            $parentProducts[$key]['price'] = $data[4];
        }
    }
}
 
function getAttributeId($option, $type) {
   $attributeId      = Mage::getResourceModel('eav/entity_attribute')->getIdByCode('catalog_product', $type);
   $attribute        = Mage::getModel('catalog/resource_eav_attribute')->load($attributeId);
   $attributeOptions = $attribute ->getSource()->getAllOptions();
   foreach ($attributeOptions as $opts_arr) {
        if ($opts_arr['label'] == $option) {
            return $opts_arr['value'];
        }
    }
    return FALSE;
}
 
function createParent($sku, $parentData, $attributeIds, $sizelist, $colourlist) {
    var_dump($parentData);
    $product = Mage::getModel('catalog/product');
    $product
        ->setTypeId(Mage_Catalog_Model_Product_Type::TYPE_CONFIGURABLE)
        ->setTaxClassId(2)
        ->setVisibility(Mage_Catalog_Model_Product_Visibility::VISIBILITY_BOTH)
        ->setStatus(Mage_Catalog_Model_Product_Status::STATUS_ENABLED)
        ->setStoreId('default')
        ->setWebsiteIDs(array(1))
        ->setAttributeSetId(4)
        ->setSku($sku)    //configure product's sku
        ->setName($parentData['name'])
        ->setPrice(sprintf("%0.2f", floatval($parentData['price'])))
        ->setCreatedAt(strtotime('now'))
        ->setStockData(
            array(
                'is_in_stock' => 1,
                'qty' => 9999
                )
            )
    ;
 
    foreach(array("gcolor", "gsize") as $attrCode){
 
        $super_attribute= Mage::getModel('eav/entity_attribute')->loadByCode('catalog_product',$attrCode);
        $configurableAtt = Mage::getModel('catalog/product_type_configurable_attribute')->setProductAttribute($super_attribute);
 
        $newAttributes[] = array(
           'id'             => $configurableAtt->getId(),
           'label'          => $configurableAtt->getLabel(),
           'position'       => $super_attribute->getPosition(),
           'values'         => $configurableAtt->getPrices() ? $configProduct->getPrices() : array(),
           'attribute_id'   => $super_attribute->getId(),
           'attribute_code' => $super_attribute->getAttributeCode(),
           'frontend_label' => $super_attribute->getFrontend()->getLabel(),
        );
    }
 
    var_dump($newAttributes);
 
    $configurableData = array();
    foreach ($parentData['childrens'] as $children) {
        $childProduct = Mage::getModel('catalog/product')->loadByAttribute('sku', $children);
        $childProduct->getId();
        preg_match('/^\d\d\d\/\d\d\d\/(\d\d\d)\/(.*)/', $children, $matches);
        $colourcode = $matches[1];
        $sizecode = $matches[2];
        $configurableData[$childProduct->getId()][] = array(
                'attribute_id' => $attributeIds['gcolor'],
                'label' => $colourlist[$colourcode],
                'value_index'=> getAttributeId($colourlist[$colourcode], 'gcolor'),
            );
        $configurableData[$childProduct->getId()][] = array(
                'attribute_id' => $attributeIds['gsize'],
                'label' => str_replace('/', '-', $sizecode),
                'value_index'=> getAttributeId(str_replace('/', '-', $sizecode), 'gsize'),
            );
    }
 
    $product->setConfigurableProductsData($configurableData);
    $product->setConfigurableAttributesData($newAttributes);
    try{
        $product->save();
        echo "added\n";
        print $product->getId()."::".$product->getSku()."\n";
    }
    catch (Exception $e){        
        echo "not added\n";
        echo "exception:$e";
    }
}
 
set_time_limit(0);
ini_set('memory_limit', '1024M');
include_once "../app/Mage.php";
 
$app = Mage::app()->setCurrentStore(Mage_Core_Model_App::ADMIN_STORE_ID);
$parentProducts = array();
 
if (($handle = fopen("newsotkc.csv", "r")) !== FALSE) {
    while (($data = fgetcsv($handle, 1000, ",")) !== FALSE) {
        collectParents($data, $parentProducts);
    }
}
 
$attributeIds = array("gcolor" => 135, "gsize" => 134);
 
$colourlist = csvreader('colour.csv');
$sizelist = csvreader('size.csv');
 
foreach ($parentProducts as $key => $parent) {
    createParent($key, $parent, $attributeIds, $sizelist, $colourlist);
}
